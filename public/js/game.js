var players = [],
    canvas = document.getElementById("canvas"),
    context = canvas.getContext("2d"),
    width = canvas.width,
    height = canvas.height,
    centerX = width / 2,
    centerY = height / 2,
    coefX = 0,
    coefY = 0,
    sw = 0,
    sh = 0,
    arenna_rayon = 0,
    spawn_rayon = 0;


var http = location.protocol;
var slashes = http.concat("//");
var host = slashes.concat(window.location.hostname) + ":" + location.port;


var socket = io.connect(host);
socket.on('start', function(data) {
    sw = data.sw;
    sh = data.sh;
    coefX = width / sw;
    coefY = height / sh;
    arenna_rayon = data.arenna_rayon;
    spawn_rayon = data.spawn_rayon;    
});


socket.on('draw', function(data) {
    context.clearRect(0, 0, width, height);
    drawArena(width / 4);

    for (var i = 0; i < data.players.length; i++) {


        context.beginPath();
        context.fillStyle = '#000000';
        context.arc(data.players[i].x * coefX, data.players[i].y * coefY, data.players[i].radius * coefX, 0, Math.PI * 2, true);
        context.fill();
        context.stroke();
        context.restore();
        context.closePath();
    }

});

function drawArena(radius) {
    context.beginPath();
    context.arc(centerX * coefX, centerY * coefY, arenna_rayon * coefX, 0, 2 * Math.PI, false);
    context.fillStyle = '#4679BD';
    context.fill();
    // context.lineWidth = 5;
    context.strokeStyle = '#4679BD';
    context.stroke();
    context.closePath();
    
    
    context.beginPath();
    context.arc(centerX * coefX, centerY * coefY, spawn_rayon * coefX, 0, 2 * Math.PI, false);
    context.fillStyle = '#e9d2d2';
    context.fill();
    // context.lineWidth = 5;
    context.strokeStyle = '#e9d2d2';
    context.stroke();
    context.closePath();
    
    
}


function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: (evt.clientX - rect.left) / coefX,
        y: (evt.clientY - rect.top) / coefY
    };
}

canvas.addEventListener('mousemove', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    console.log(mousePos);
    socket.emit('mousemove', {
        mouse: mousePos
    });

}, false);