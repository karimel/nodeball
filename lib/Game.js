var Room = require('./game/Room.js');

function Game(output) {

    var self = this;
    var io = output;
    var rooms = [];




    function initListenner() {

        io.on('connection', function(socket) {
            var size = rooms.length,
                currentRoom;
            for (var i = 0; i < size; i++) {
                if (!rooms[i].hasCompleted()) {
                    currentRoom = rooms[i];
                    break;
                }
            }
            if (typeof currentRoom === typeof undefined) {
                currentRoom = new Room(io);
                rooms.push(currentRoom);
                currentRoom.run();
            }
            currentRoom.addPlayer(socket);
        });
    }
    initListenner();
}
module.exports = function(io) {
    return new Game(io);
};