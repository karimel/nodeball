var config = require('./Config.js'),
    Utils = require('./Utils.js'),
    vector = require('./Vector.js');



function Collidable() {
    var self = this;
    self.id = Utils.guid();

    self.position = vector.create(config.WIDTH / 2, config.HEIGHT / 2);
    self.velocity = vector.create(0, 0);
    self.friction = 0.970;
    self.velocity.setLength(0);
    self.velocity.setAngle(0);

    self.thrust = vector.create(0, 0);
    self.angle = 0;
    self.mass = 1;
    self.turningLeft = false;
    self.turningRight = false;
    self.thrusting = false;
    self.radius = 10;
    self.mouseX = config.WIDTH / 2;
    self.mouseY = config.HEIGHT / 2;
    self.color = "red"; 
    self.type = "collide";

    self.accelerate = function(accel) {
        this.velocity.addTo(accel);
    };
    self.applyFriction = function() {
        if (this.velocity.getLength() < 0.250) {
            this.velocity.setLength = 0;
        }
        this.velocity.multiplyBy(this.friction);
    };
    self.destroy = function() {};
    self.setMass = function(inMass) {
        this.mass = inMass;
    };
    self.getMass = function() {
        return this.mass;
    };
    self.update = function() {
        this.position.addTo(this.velocity);
        this.applyFriction();
    };

    self.data = function() {
        return {
            x: this.position.getX(),
            y: this.position.getY(),
            color: this.color,
            radius: this.radius
        };
    };
 
    self.isColliding = function(ball2) {
        var colliding = false;
        if (self.getDistanceBetweenParticle(ball2) <= self.radius + ball2.radius) {
            colliding = true;
        }
        return colliding;
    }

    self.getDistanceBetweenParticle = function(particle2) {
        var distX = self.position.getX() - particle2.position.getX(),
            distY = self.position.getY() - particle2.position.getY();
        return Math.sqrt(distX * distX + distY * distY) - (self.radius + particle2.radius);
    }

    self.collide = function(ball2) {
        
        var xDistance = (ball2.position.getX() - self.position.getX());
        var yDistance = (ball2.position.getY() - self.position.getY());

        var normalVector = vector.create(xDistance, yDistance); // normalise this vector store the return value in normal vector.
        normalVector = normalVector.normalise();

        var tangentVector = vector.create((normalVector.getY() * -1), normalVector.getX());

        // create ball scalar normal direction.
        var ball1scalarNormal = normalVector.dot(self.velocity);
        var ball2scalarNormal = normalVector.dot(ball2.velocity);

        // create scalar velocity in the tagential direction.
        var ball1scalarTangential = tangentVector.dot(self.velocity);
        var ball2scalarTangential = tangentVector.dot(ball2.velocity);

        var ball1ScalarNormalAfter = (ball1scalarNormal * (self.getMass() - ball2.getMass()) + 2 * ball2.getMass() * ball2scalarNormal) / (self.getMass() + ball2.getMass());
        var ball2ScalarNormalAfter = (ball2scalarNormal * (ball2.getMass() - self.getMass()) + 2 * self.getMass() * ball1scalarNormal) / (self.getMass() + ball2.getMass());

        var ball1scalarNormalAfter_vector = normalVector.multiply(ball1ScalarNormalAfter); // ball1Scalar normal doesnt have multiply not a vector.
        var ball2scalarNormalAfter_vector = normalVector.multiply(ball2ScalarNormalAfter);

        var ball1ScalarNormalVector = (tangentVector.multiply(ball1scalarTangential));
        var ball2ScalarNormalVector = (tangentVector.multiply(ball2scalarTangential));

        self.velocity = ball1ScalarNormalVector.add(ball1scalarNormalAfter_vector);
        
        if (ball2.type != "spawn")
            ball2.velocity = ball2ScalarNormalVector.add(ball2scalarNormalAfter_vector);

    }  
}

function Player(socket) {
    var self = this;
    
    Utils.extends(self,new Collidable());
    
    self.socket = socket;

    self.OnDisconnect = null;
    self.death = false;
    self.spawn = true;
    self.radius = 30;

    self.dispose = function() {
        socket.removeAllListeners("mousemove");
        socket.removeAllListeners("disconnect");
        socket.removeAllListeners("join");
        self.OnDisconnect = null;
    };
    socket.on('mousemove', function(data) {
        self.mouseX = data.mouse.x;
        self.mouseY = data.mouse.y;
    });
    socket.on('disconnect', function() {
        if (typeof self.OnDisconnect === 'function')
            self.OnDisconnect.call(null, self);
        self.dispose();
    });
}

module.exports = {
    Collidable:Collidable,
    Player:Player
};