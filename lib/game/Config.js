module.exports = {
    MAX_PLAYER_BY_ROOM: 100,
    WIDTH: 500,
    HEIGHT: 500,
    ARENNA_RAYON: 250,
    SPAWN_RAYON: 50
};