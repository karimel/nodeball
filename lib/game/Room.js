var config = require('./Config.js'),
    Collidable = require('./Player.js').Collidable,
    Player = require('./Player.js').Player,
    Utils = require('./Utils.js'),
    vector = require('./Vector.js');

function Room(io) {

    var self = this,
        players = [],
        running = false,
        sessionInterval = null;

    self.id = Utils.guid();
    
    self.spawn = new Collidable();
    self.spawn.type = "spawn";
    self.spawn.radius = config.SPAWN_RAYON;
    self.spawn.setMass(100000000);
    
    
    self.hasCompleted = function() {
        if (players.length > config.MAX_PLAYER_BY_ROOM) {
            return true;
        }
        return false;
    };
    self.addPlayer = function(socket) {
        socket.join(self.id);
        var player = new Player(socket);
        players.push(player);
        player.OnDisconnect = function(player) {
            for (var i = 0; i < players.length; i++) {
                if (players[i].id == player.id) {
                    players.splice(i, 1);
                    break;
                }
            }
        };
        socket.emit('start', {
            id: self.id,
            sw: config.WIDTH,
            sh: config.HEIGHT,
            arenna_rayon: config.ARENNA_RAYON,
            spawn_rayon: config.SPAWN_RAYON,
        });
    };
    self.run = function() {
        if (running) return;
        running = true;
        sessionInterval = setInterval(step, 16);
    };
    self.stop = function() {
        if (!running) return;
        clearInterval(sessionInterval);
        running = false;
    };
    function step() {
        var data = [];
        for (var i = 0; i < players.length; i++) {
            var ball = players[i];
            ball.thrust.setAngle(ball.angle);

            if (ball.thrusting) {
                ball.thrust.setLength(0.3);
            } else {
                ball.thrust.setLength(0);
            }

            var distX = ball.mouseX - ball.position.getX(),
                distY = ball.mouseY - ball.position.getY();
            var dist = Math.sqrt(distX * distX + distY * distY);
            if (dist > ball.radius) {
                ball.thrusting = true;
                ball.angle = Math.atan2(distY, distX);
            } else {
                ball.thrusting = false;
            }

            ball.accelerate(ball.thrust);
            ball.update();
            if(players.length > 1) {
                if (ball.isColliding(self.spawn)) {
                    if (!ball.spawn) {
                        console.log('collision'+ball.position.getX()+'_'+ball.position.getY());
                        ball.collide(self.spawn);
                    }
                } else {
                    if (ball.spawn) {
                        ball.spawn = false;
                        data.push(players[i].data());
                        continue;
                    }
                }
            }
            for (var z = 0; z < players.length; z++) {
                    if (players[z].id === ball.id || players[z].spawn) continue;
                    if (ball.isColliding(players[z])) {
                        ball.collide(players[z]);
                    }
            }
            data.push(players[i].data());
        }
        io.to(self.id).emit('draw', {
            players: data
        });

    }
}


module.exports = Room;