var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);


var game = require('./lib/Game.js')(io);


app.use(express.static('public'));


server.listen(5000);